const http = require('http')
    , app = require('./app')
    , port = process.env.PORT || 1600
    , server = http.createServer(app)

server.listen(port)
console.log(`listen  http://localhost:${port}/api/`)