'use strict'

const express = require('express')
    , morgan = require('morgan')
    , bodyParser = require('body-parser')
    , mongoose = require('mongoose')
    , app = express()

const routes = require('./routes')

mongoose.connect('mongodb+srv://root:root@clubmin-tfbbz.gcp.mongodb.net/test?retryWrites=true', { useNewUrlParser: true })
mongoose.set('useCreateIndex', true);

app.use(morgan('dev'))
    .use(bodyParser.urlencoded({extended: false}))
    .use(bodyParser.json())
    .use((req, res, next) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Methods", "GET,HEAD,OPTIONS,POST,PUT");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");

        if(req.method === 'OPTIONS') {
            res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET')

            return res.status(200).json({})
        }

        next();
    })
    .use(...routes)

app.use((req, res, next) => {
    const error = new Error('not found');

    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error
    })
})

module.exports = app