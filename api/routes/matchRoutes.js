const express = require('express')
    , mongoose = require('mongoose')
    , Match = require('../models/match')
    , router = express.Router()

router.post('/match', (req, res) => {
    if(req.body.player1 === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player 1 name must be set."})
    }

    if(req.body.player1.length === 0) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player 1 name length is incorrect."})
    }

    if(req.body.player2 === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player 2 name must be set."})
    }

    if(req.body.player2.length === 0) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player 2 name length is incorrect."})
    }

    const match = new Match({
        _id: new mongoose.Types.ObjectId(),
        player1: req.body.player1,
        player2: req.body.player2,
        status: 'inprogress',
        winner: null
    })

    const r = match.save()

    r.then(result => {
        const {_id, player1, player2 , status , winner} = result

        return res.status(200).json({id:_id, player1, player2 , status , winner})
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

router.patch('/match/:id', (req, res) => {
    const _id = req.params.id
        , status = req.body.status
        , winner = req.body.winner
    
    let consult = {}

    if(status !== undefined) {
        consult = {...consult, status}
    }

    if(winner !== undefined) {
        consult = {...consult, winner}
    }

    if(Object.keys(consult).length === 0) {
        res.status(500).json({error: 'No arguments provied', message: "U have to pass at least one argument."})
    }

    const r = Match.updateOne({_id}, {$set: {...consult}})

    r.then(result => {
        const { _id, player1, player2, winner, status } = result 

        return res.status(200).json({id: _id, player1, player2, winner, status})
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

router.get('/match/:id', (req, res) => {
    if(req.query.id === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "match id is required"})
    }

    const id = req.query.id
        , p = Match.findById(id)

    p.then(result => {
        if(result) {
            const {_id, player1, player2, winner, status} = result

            res.status(200).json({id: _id, player1, player2, winner, status})
        } else {
            return res.status(400).json({message: "not valid entry found for provided id"})
        }
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

module.exports = router