const express = require('express')
    , { getScoresByUsers }  = require('../helpers/scoresHelpers')
    , Player = require('../models/player')
    , Match = require('../models/match')
    , router = express.Router()


router.get('/scores', (req, res) => {
    const p = Player.find()

    p.then(result => {
        if(result.length > 0) {
            return getScoresByUsers(result, Match)
        } else {
            return []
        }
    }).then(data => {
        return res.status(200).json(data)
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

module.exports = router