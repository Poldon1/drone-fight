const express = require('express')
    , mongoose = require('mongoose')
    , Player = require('../models/player')
    , router = express.Router()
    
router.post('/players', (req, res) => {
    if(req.body.name === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player name must be set."})
    }

    if(req.body.name.length === 0 || req.body.name.length > 9) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player name length is incorrect."})
    }

    const player = new Player({
        _id: new mongoose.Types.ObjectId(),
        name: req.body.name
    })

    const r = player.save()

    r.then(result => {
        const {_id, name} = result

        return res.status(200).json({id:_id, name})
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

router.get('/players/:id', (req, res) => {
    if(req.query.id === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "player id is required"})
    }

    const id = req.query.id
        , p = Player.findById(id)

    p.then(result => {
        if(result) {
            const {_id, name} = result

            res.status(200).json({id:_id, name})
        } else {
            return res.status(400).json({message: "not valid entry found for provided id"})
        }
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

router.get('/playersExist', (req, res) => {
    if(req.query.name === undefined) {
        return res.status(500).json({error: "invalid arguments", message: "nop men!!, player name must be set."})
    }

    const {name} = req.query
        , p = Player.find({name})

    p.then(result => {
        if(result) {
            if(result.length === 0) 
                return  res.status(200).json({})
                
            const {_id, name} = result[0]

            res.status(200).json({id:_id, name})
        } else {
            return res.status(400).json({message: "not valid entry found for provided id"})
        }
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

router.delete('/players/:id', (req, res) => {
    if(req.params.id === undefined) {
        res.status(500).json({error: "invalid arguments", message: "player id is required"})
    }
    
    const _id = req.params.id

    const p = Player.remove({_id})

    p.then(result => {
        return res.status(200).json(result)
    }).catch(err => {
        console.log(err)
        return res.status(500).json({error:err, message: "houston we have a problem"})
    })
})

module.exports = router