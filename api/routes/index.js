const mainRoutes = require('./mainRoutes')
    , playerRoutes = require('./playersRoutes')
    , matchRoutes = require('./matchRoutes')
    , scoresRoutes = require('./scoresRoutes')
    , mainRoot = '/api/';

module.exports = [
    mainRoot,
    mainRoutes,
    playerRoutes,
    matchRoutes,
    scoresRoutes
]