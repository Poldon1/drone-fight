const express = require('express')
    , path = require('path')
    , router = express.Router()
    
router.get('/', (req, res, next) => {
    try{
        res.sendFile(path.join(__dirname+'/../view.html'))
    } catch(err) {
        res.status(500).json({
            error: 'something`s wrong with the main view',
        })
    }
})

module.exports = router