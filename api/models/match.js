const mongoose = require('mongoose')

const matchSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    player1:[
        {type: mongoose.Schema.Types.ObjectId, ref: 'Player'}
    ],
    player2:[
        {type: mongoose.Schema.Types.ObjectId, ref: 'Player'}
    ],
    status: String,
    winner: [
        {type: mongoose.Schema.Types.ObjectId, ref: 'Player'}
    ]
})

module.exports = mongoose.model('Match', matchSchema)