const mongoose = require('mongoose')

const playerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: { type: String, unique: true}
})

module.exports = mongoose.model('Player', playerSchema)