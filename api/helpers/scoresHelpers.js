const getScoresByUsers = async (players, Match) => {
    let result = []

    for (let i = 0; i < players.length; i++) {
        let wins = await Match.find({ winner: players[i]._id }).then(match => {
            return match.length
        }).catch(err => {
            throw err
        })

        const element = {id: players[i]._id, name: players[i].name, wins}
        result.push(element)
    }

    return result
}

module.exports = { getScoresByUsers }