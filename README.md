This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

##Set Up
``npm install``

##Run Api
``npm run api``

##Run front
``npm start``

Must open two consoles one for the api and one for the front.
