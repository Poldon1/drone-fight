import enterPlayers from '../components/form/enterPlayers'
import React from 'react';
import renderer from 'react-test-renderer';

describe("Test enterPlayers component render", () => {
    it("render correctly enterPlayers component", () => {
        const rendered = renderer.create(
            <enterPlayers/>
        );

        expect(rendered.toJSON()).toMatchSnapshot();
    })
})