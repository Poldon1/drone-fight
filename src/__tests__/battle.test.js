import { BattleVeil, ROCK, PAPER, SISSORS, PLAYER_1, PLAYER_2 } from '../components/battle/battle'

describe("Test battle component, retunr calculate callback for the game logic", () => {
    it("test game logic", async () => {
        let data = {choices: [ROCK, SISSORS], callback: r =>{
            expect(r).toEqual(PLAYER_1)
        }}

        await BattleVeil(data)
        data = {choices: [SISSORS, ROCK], callback: r =>{
            expect(r).toEqual(PLAYER_2)
        }}
        await BattleVeil(data)
        data = {choices: [ROCK, PAPER], callback: r =>{
            expect(r).toEqual(PLAYER_2)
        }}
        await BattleVeil(data)
        data = {choices: [PAPER, ROCK], callback: r =>{
            expect(r).toEqual(PLAYER_1)
        }}
        await BattleVeil(data)
        data = {choices: [PAPER, SISSORS], callback: r =>{
            expect(r).toEqual(PLAYER_2)
        }}
        await BattleVeil(data)
    })
})