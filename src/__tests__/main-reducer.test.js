import  { mainReducer, ADD_PLAYER, REMOVE_PLAYER, BEGAN_MATCH, SET_WINNER, RESET } from '../contexts/reducers/main-reducer'

describe('testing Main reducer Actions', () => {
    it("must add player to the state", () => {
        const state = {players: [null, null]}
            , data1 = {type: ADD_PLAYER, player: {name: "jables", id: 0}}
            , data2 = {type: ADD_PLAYER, player: {name: "cage", id: 1}}
    
        const r1 = mainReducer(state, data1)
        expect(r1).toEqual({players: ['jables', null]})
    
        const r2 = mainReducer(state, data2)
        expect(r2).toEqual({players: ['jables', 'cage']})
    })
    
    it("must return and error on inject repeat player", () => {
        const state = {players: ['jables', null]}
            , data = {type: ADD_PLAYER, player: {name: "jables", id: 0}}
    
        const r = mainReducer(state, data)
        expect(r.error !== undefined).toBeTruthy()
    })
    
    it("must delete player by passing correct id", () => {
        const state = {players: ['jables', 'cage']}
            , data = {type: REMOVE_PLAYER, playerId: 0}
    
        const r = mainReducer(state, data)
        expect(r).toEqual({players: [null, 'cage']})
    })
    
    it("must change the match state", () => {
        const state = {matchBegan: false}
            , data = {type: BEGAN_MATCH, players: ['jables', 'cage'] }
    
        const r = mainReducer(state, data)
        expect(r).toEqual({ matchBegan: [ 'jables', 'cage' ] })
    })
    
    it("must set the winner object", () => {
        const state = {winner: {}}
            , data = {type: SET_WINNER, winner: {
                name: "jables",
                url: "img/url",
                key: "player1",
                wins: 3,
                choice: ''
            }}
    
        const r = mainReducer(state, data)
    
        expect(r).toEqual({ winner:
            { name: 'jables',
              url: 'img/url',
              key: 'player1',
              wins: 3,
              choice: '' }
            })
    })
    
    it("must reset all values of the reducer", () => {
        const state = {
                winner: { 
                    name: 'jables',
                    url: 'img/url',
                    key: 'player1',
                    wins: 3,
                    choice: ''
                },
                matchBegan: true,
                players: ['jables', 'cage']
            }
            , data = {type:RESET, state: {players: [null, null], matchBegan: false, winner: {}}}
    
        const r = mainReducer(state, data)
    
        expect(r).toEqual({ winner: {}, matchBegan: false, players: [ null, null ] })
    })
})