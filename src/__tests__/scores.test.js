import Scores from '../components/scores/scores'
import React from 'react';
import renderer from 'react-test-renderer';

describe("Test scores component render", () => {
    it('render correctly scores component', () => {
        const data = {tittle:"test", rowTitles: ["title"], data: {data: ["---"]}}
        const rendered = renderer.create(
            <Scores props={data} />
        );

        expect(rendered.toJSON()).toMatchSnapshot();
    });
})