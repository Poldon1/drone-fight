import GlobalState from '../contexts/states/globalState'
import React from 'react';
import renderer from 'react-test-renderer';

describe("Testing Global provider rendering", () => {
    it('render correctly provider component', () => {  
        const rendered = renderer.create(
            <GlobalState />
        );

        expect(rendered.toJSON()).toMatchSnapshot();
    });
})

