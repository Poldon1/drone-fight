import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'

import GlobalState from './contexts/states/globalState'

import HomePage from './pages/home/home'
import ArenaPage from './pages/arena/arena'
import FinishPage from './pages/finish/finish'

import './App.scss';

const App = props => {
  return (
    <GlobalState>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={HomePage} exact />
          <Route path="/arena" component={ArenaPage} exact />
          <Route path="/finish" component={FinishPage} exact />
        </Switch>
      </BrowserRouter>
    </GlobalState>
  );
}

export default App
