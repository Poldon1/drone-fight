import React from 'react'

export default React.createContext({
    players: [null, null],
    matchBegan: [],
    winner: {},
    alerts: [],
    shooteAlert: alerts => {},
    resetAlerts: () => {},
    addPlayer: player => {},
    removePlayer: playerId => {},
    beganMatch: players => {},
    setWinner: winner => {},
    reset: () => {},
    restoreMatch: match => {}
})