import React, { useReducer } from 'react'
import MainContext from '../context/main-context'
import  { 
    mainReducer, 
    ADD_PLAYER, 
    REMOVE_PLAYER, 
    BEGAN_MATCH, 
    SET_WINNER, 
    RESET, 
    ALERT, 
    RESET_ALERTS,
    RESTORE_MATCH
} from '../reducers/main-reducer'

import Alert from '../../components/alert/alert' 
import mainSong from '../../assets/sounds/laser.mp3'

const GlobalState = props => {
    const players = [null, null]
        , matchBegan = false
        , winner = {}
        , alerts = []

    const [globalState, dispatch] = useReducer(mainReducer, {players, matchBegan, winner, alerts})

    const actions = {
        addPlayer: player => dispatch({type: ADD_PLAYER, player}),
        removePlayer: playerId => dispatch({type: REMOVE_PLAYER, playerId}),
        beganMatch: players => dispatch({type: BEGAN_MATCH, players}),
        setWinner: winner => dispatch({type: SET_WINNER, winner:winner}),
        reset: () => dispatch({type:RESET, state: {players: [null, null], matchBegan: false, winner: {}, alerts: []}}),
        shooteAlert: alerts => dispatch({type:ALERT, alerts}),
        resetAlerts: () => dispatch({type: RESET_ALERTS}),
        restoreMatch: match => dispatch({type: RESTORE_MATCH, match})
    }

    const shootingStarts = () => {
        let starts = []

        for(let i=0; i < 5; i++) {
            starts.push(<div key={i} className="p p-1"></div>)
            starts.push(<div key={i + "e"} className="p p-2"></div>)
            starts.push(<div key={i + "c"} className="p p-3"></div>)
        }

        return (<div className="nigth">{starts.map(e => e)}</div>)
    }

    return (
        <MainContext.Provider
            value={{
                players: globalState.players,
                matchBegan: globalState.matchBegan,
                winner: globalState.winner,
                alerts: globalState.alerts,
                ...actions
            }}
        >
        {shootingStarts()}
        {props.children}
        <div style={{display:'none'}}>
            <audio src={mainSong} autoPlay/>
        </div>
        {Alert({alerts: globalState.alerts, resetAlerts: actions.resetAlerts})}
      </MainContext.Provider>
    )
}

export default GlobalState