export const ADD_PLAYER = 'ADD_PLAYER'
export const REMOVE_PLAYER = 'REMOVE_PLAYER'
export const BEGAN_MATCH = 'BEGAN_MATCH'
export const SET_WINNER = 'SET_WINNER'
export const RESET = "RESET"
export const ALERT = "ALERT"
export const RESET_ALERTS = "RESET_ALERTS"
export const RESTORE_MATCH = "RESTORE_MATCH"

export const mainReducer = (state, action) => {
    switch(action.type) {
        case ADD_PLAYER: 
            let p = state.players

            if(p.indexOf(action.player.name) !== -1) 
                return {...state, error: `Player ${action.player.name} allready in the match`}

            p[action.player.id] = action.player.name

            return {...state, players: p}
        case REMOVE_PLAYER: 
            let rp = state.players

            rp[action.playerId] = null

            return {...state, players: rp}
        case BEGAN_MATCH: 
            return {...state, matchBegan: action.players}
        case SET_WINNER: 
            return {...state, winner: action.winner}
        case ALERT: 
            const {alerts} = action
            
            return {...state, alerts: [...state.alerts, alerts]}
        case RESET_ALERTS: 
            return {...state, alerts: []}
        case RESTORE_MATCH: 
            const {match} = action

            return {...state, matchBegan: match}
        case RESET:
            return {...state, ...action.state}
        default:
            return state
    }
}