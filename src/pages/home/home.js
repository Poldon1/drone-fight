import React, { useContext, useEffect, useState } from 'react'

import MainContext from '../../contexts/context/main-context'
import PlayerForm from '../../components/form/enterPlayers'
import Scores from '../../components/scores/scores'

import { setMatch } from '../../services/matches'
import { getScores } from '../../services/scores'

const HomePage = props => {
    const context = useContext(MainContext)
        , { matchBegan, addPlayer, beganMatch, shooteAlert, restoreMatch } = context
        , [ isLoading, setIsLoading ] = useState(false)
        , [ score , setScore ] = useState({
            tittle: "Global Scores"
        })

    useEffect(() => {
       let matchInprocess = JSON.parse(localStorage.getItem('currentMatch'))

       if(matchInprocess !== null && matchInprocess.status !== 'finish') {    
            updateDate(matchInprocess).then(() => {
                props.history.push('/arena')
            }).catch(err => {
                context.shooteAlert(['something went wrong'])
                throw err
            })
       }

       setIsLoading(true)
       getScores().then(res => {
            let s = {
                ...score
                , data: {
                    rowTitles: ["name", "wins"],
                    data: res.map(data => {
                        return [data.name,data.wins]
                    })
                }
            }

            setScore(s)
            setIsLoading(false)
       }).catch(err => {
            setIsLoading(false)
            context.shooteAlert(['something went wrong'])
            throw err
       })
    }, [])

    useEffect(() => {
        if(matchBegan.length > 0) {
            setMatch({player1: matchBegan[0].user, player2: matchBegan[1].user}).then(res => {
                const cache = {...res, player1: matchBegan[0], player2: matchBegan[1]}
                localStorage.setItem('currentMatch', JSON.stringify(cache))
                restoreMatch(cache)

                props.history.push('/arena')
            })
        }
    }, [matchBegan])

    const updateDate = async (match) => {
        restoreMatch(match)
        addPlayer(match.player1)
        addPlayer(match.player2)

        return;
    }

    return (
        <React.Fragment>
            <div className="container">
                <h1 className="text-center">This Is Not the Greates <br /> Game In The World</h1>
                <h4 className="text-center tittle-blink">This is just a tribute !!!</h4>
                {PlayerForm({addPlayer, beganMatch, shooteAlert })}
                { isLoading ? "...Loading scores" : Scores(score)}
            </div>
        </React.Fragment>
    )
}

export default HomePage