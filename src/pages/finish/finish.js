import React, { useContext, useEffect, useState } from 'react'
import MainContext from '../../contexts/context/main-context'
import { setMatch } from '../../services/matches'

import endSong from '../../assets/sounds/end.mp3'

import jables from '../../assets/img/jables.png'

import './finish.scss'

const FinishPage = props => {
    const context = useContext(MainContext)
        , [ isLoading, setIsLoading ] = useState(false)

    useEffect(() => {
        if(context.players.includes(null) && !context.matchBegan) {
            props.history.push('/')
        }
    }, [])

    const resetContext = () => {
        setIsLoading(true)
        context.reset()
        setIsLoading(false)
        props.history.push('/')
    }

    const replay = () => {
        setIsLoading(true)

        const oldMatch = context.matchBegan

        context.setWinner({})
        context.beganMatch()

        setMatch({player1: oldMatch.player1.user, player2: oldMatch.player2.user}).then(res => {
            const cache = {...res, player1: oldMatch.player1, player2: oldMatch.player2}
            localStorage.setItem('currentMatch', JSON.stringify(cache))
            context.restoreMatch(cache)

            setIsLoading(false)
            props.history.push('/arena')
        }).catch(err => {
            setIsLoading(false)
            context.shooteAlert(['something went wrong'])
            throw err
        })
    }

    return <div className="container">
        <div className="finish">
            <h1 className="text-center">We have a Winner!!</h1>
            <h4 className="text-center"><span className="tittle-blink">{context.winner.name}</span> is our new Jables!!</h4>
            <h3 className="text-center">
                <span className="tittle-blink">¯\_(ツ)_/¯</span>
            </h3>
            <div className="actions">
                <button className={isLoading ? 'disable' : ''} 
                    disabled={isLoading} onClick={() => replay()}>{isLoading? "...loading" : "And Other Ride ?"}</button>
                <button  className={isLoading ? 'disable' : ''} 
                    disabled={isLoading} onClick={() => resetContext()}>{ isLoading? "...loading" : "choose other players"}</button>
            </div>
            <img className="jables" alt="jables" src={jables} />
        </div>
        <div style={{display:'none'}}>
            <audio src={endSong} autoPlay/>
        </div>
    </div>
}

export default FinishPage