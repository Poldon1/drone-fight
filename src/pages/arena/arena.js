import React, { useContext, useEffect, useState } from 'react'
import MainContext from '../../contexts/context/main-context'

import { useShipRamdom } from '../../hooks/useShipRamdom'

import { ROCK, PAPER, SISSORS, BattleVeil, PLAYER_1, PLAYER_2 } from '../../components/battle/battle'
import Scores from '../../components/scores/scores'

import { updateMatch } from '../../services/matches'

import startSong from '../../assets/sounds/sound.mp3'

import './arena.scss'

const ArenaPage = props => {
    const context = useContext(MainContext)
        , [ player1Ship ] = useShipRamdom()
        , [ player2Ship ] = useShipRamdom()
        , [ round, setRound ] = useState(1)
        , [ action, setAction ] = useState(false)
        , [ scores, setScores ] = useState({
            rowTitles: ['Round', 'Winner'],
            data: [['---', '---']]
        })
        , [ fullPlayes, setFullPlayers ] = useState([
            {
                name: context.players[0],
                url: player1Ship,
                key: "player1",
                wins: 0,
                choice: ''
            },
            {
                name: context.players[1],
                url: player2Ship,
                key: "player2",
                wins: 0,
                choice: ''
            }
        ])
        , [ turnOf, setTurnOf ] = useState(fullPlayes[0])

    let score = {
        tittle: "Scores",
        class: "arenaScore",
        data: scores
    }

    useEffect(() => {
        if(context.players.includes(null) && !context.matchBegan) {
            props.history.push('/')
        }

        if(Object.keys(context.winner).length > 0) {
            props.history.push('/finish')
        }
    }, [])

    useEffect(() => {
        let playersUpdate = fullPlayes

        if(playersUpdate[0].wins === 3) {
            finishMatch(context.matchBegan.id, context.matchBegan.player1.user).then(res => {
                context.setWinner(playersUpdate[0])

                return props.history.push('/finish')
            })
        }

        if(playersUpdate[1].wins === 3) {
            finishMatch(context.matchBegan.id, context.matchBegan.player2.user).then(res => {
                context.setWinner(playersUpdate[1])

                return props.history.push('/finish')
            })
        }

        if(turnOf.choice.length > 0 && turnOf.key === "player1") {    
            playersUpdate[0] = turnOf;
            setFullPlayers(playersUpdate)
            setTurnOf(fullPlayes[1])
        } else if(turnOf.choice.length > 0 && turnOf.key === "player2") {
            playersUpdate[1] = turnOf
            setFullPlayers(playersUpdate)

            executeRound()
        }
    }, [turnOf])

    const finishMatch = async (matchId, winner) => {
        await updateMatch(matchId, {status: 'finish', winner}).catch(err => {
            context.shooteAlert(['something went wrong'])
            throw err
        })

        localStorage.removeItem("currentMatch")

        return;
    }

    const executeRound = () => {
        const [player1, player2] = fullPlayes
            , consult = {
                choices: [player1.choice, player2.choice],
                callback: (winner) => {
                    let s = scores.data

                    if(winner === PLAYER_1) {
                        let players = fullPlayes

                        players[0].wins += 1
                        players[0].choice = ''
                        players[1].choice = ''
                        scores.data.unshift([round, players[0].name])
                        setFullPlayers(players)
                    } else if(winner === PLAYER_2) {
                        let players = fullPlayes

                        players[1].wins += 1
                        players[1].choice = ''
                        players[0].choice = ''
                        scores.data.unshift([round, players[1].name])
                        setFullPlayers(players)
                    } else {
                        let players = fullPlayes
                        
                        players[1].choice = ''
                        players[0].choice = ''
                        setFullPlayers(players)
                    }

                    let newRound = round
                    newRound += 1 

                    setScores({...scores, data: s})
                    setRound(newRound)
                    setTurnOf(fullPlayes[0])
                }
            };

        BattleVeil(consult)
    }

    const showMarker = wins => {
        let element = new Array(3)

        for (let i = 0; i < wins; i++) {
            if(i < 3) {
                element[i] = 0
            }
        }

        return element.map((e, i) => <span key={i + "table"} className={`bar ${e !== null? "full" : ""}`}></span>)
    }

    const createShip = props => {
        return <div className="player" key={props.key}>
            <label>CHALENGER {props.name}</label>
            <div className="wins">
                {showMarker(props.wins)}
            </div>
            <div className="ship"><img alt={props.name} src={props.url} /></div>
        </div>
    }

    const panel = props => {
        return <div className="panel">
            <label>{props.name} TURN </label>
            <div className={`moves-bubble ${props.key === "player1"? "blue" : "red"} ${action? "active": ""}`} onClick={() => setAction(!action)}>
                <span className="rock" onClick={() => setTurnOf({...props, choice: ROCK})}></span>
                <span className="paper" onClick={() => setTurnOf({...props, choice: PAPER})}></span>
                <span className="sissors" onClick={() => setTurnOf({...props, choice: SISSORS})}></span>
            </div>
        </div>
    }

    return (
        <React.Fragment>
            <div className="board">
                <h1 className="text-center">ROUND {round}</h1>
                <div className="ship-block">{fullPlayes.map((p) => createShip(p))}</div>
                {panel(turnOf)}
                {Scores(score)}
                <div style={{display:'none'}}>
                    <audio src={startSong} autoPlay/>
                </div>
            </div>
        </React.Fragment>
    )
}

export default ArenaPage