import axios from 'axios'
import { url, MATCHES } from './constants'

export const setMatch = (match) => {
    return axios.post(url(MATCHES), {...match}).then(res => {
        return res.data
    })
}

export const updateMatch = (id, data) => {
    return axios.patch(url(`${MATCHES}/${id}`), {...data}).then(res => {
        return res.data
    })
}

export const getMatch = (id) => {
    return axios.get(url(MATCHES), {params: {id}}).then(res => {
        return res.data
    })
}