export const API = "http://localhost:1600/api/"
export const PLAYERS = "players"
export const PLAYER_EXIST = "playersExist"
export const MATCHES = "match"
export const SCORES = "scores"
export const url = endpoint => `${API}${endpoint}`
