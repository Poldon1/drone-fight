import axios from 'axios'
import { url, PLAYERS, PLAYER_EXIST } from './constants'

export const setPlayer = (player) => {
    return axios.post(url(PLAYERS), {...player}).then(res => {
        return res.data
    })
}

export const getPlayer = (name) => {
    return axios.get(url(PLAYER_EXIST), {params: {name}}).then(res => {
        return res.data
    })
}