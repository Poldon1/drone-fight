import axios from 'axios'
import { url, SCORES } from './constants'

export const getScores = () => {
    return axios.get(url(SCORES)).then(data => {
        return data.data
    })
}