import React from 'react'

import './scores.scss'

const Scores = props => {
    return <div className={`scores ${props.class !== undefined? props.class : ""}`}>
        <h4>{props.tittle}</h4>
        {props.data !== undefined && props.data.data.length > 0? table(props.data) : ""}
    </div>
}

const table = props => {
    return <table>
        <thead>
            <tr>
                {props.rowTitles.map(head => <th key={head}>{head}</th>)}
            </tr>
        </thead>
        <tbody>
            {props.data.map((row, i) => <tr key={i + "row"}>
                {row.map((column, ind) => <td key={i + ind + "colum"}>{column}</td>)}
            </tr>)}
        </tbody>
    </table>
}

export default Scores