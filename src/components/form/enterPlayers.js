import React, { useState, useEffect } from "react";
import { useInput } from '../../hooks/useInput';
import { getPlayer, setPlayer } from '../../services/players'

import './enterPlayers.scss'

const PlayerForm = props => {
    const [ player1, bindPlayer1 ] = useInput('')
        , [ player2, bindPlayer2 ] = useInput('')
        , [ alert, setAlert ] = useState([])
        , [ isLoad, setIsLoad ] = useState(false)
        , { addPlayer, beganMatch, shooteAlert } = props

    useEffect(() => {
        let count = 0
            , alerts = []

        if(player1 === player2 && player1.length > 0 && player2.length > 0) {
            count++
            alerts = [...alerts, {alert: true, message: 'the players can be the same', type: 'error'}]
        }

        if(player1.indexOf(' ') !== -1) {
            count++
            alerts = [...alerts, {alert: true, message: 'Player 1, sorry your name can`t have spaces', type: 'error'}]
        }

        if(player2.indexOf(' ') !== -1) {
            count++
            alerts = [...alerts, {alert: true, message: 'Player 2, sorry your name can`t have spaces', type: 'error'}]
        }

        if(count === 0) {
            setAlert([])
        } else {
            setAlert(alerts)
        }
    }, [ player1, player2 ])

    const fetchPlayers = async (players) => {
        setIsLoad(true)

        let player1 = await getPlayer(players[0]).catch(err => {
                shooteAlert(['server error try later'])
                setIsLoad(false)
                throw err
            })
            , r = [null, null]

        if(Object.keys(player1).length === 0) {
            player1 = await setPlayer({name: players[0]}).catch(err => {
                shooteAlert(['server error try later'])
                setIsLoad(false)
                throw err
            })

            r[0] = {name: player1.name, id: 0, user: player1.id}
        } else {
            r[0] = {name: player1.name, id: 0, user: player1.id}
        }

        let player2 = await getPlayer(players[1]).catch(err => {
            shooteAlert(['server error try later'])
            setIsLoad(false)
            throw err
        })

        if(Object.keys(player2).length === 0) {
            player2 = await setPlayer({name: players[1]}).catch(err => {
                shooteAlert(['server error try later'])
                setIsLoad(false)
                throw err
            })
            r[1] = {name: player2.name, id: 1, user: player2.id}
        } else {
            r[1] = {name: player2.name, id: 1, user: player2.id}
        }

        setIsLoad(false)
        return r
    }

    const handleSubmit = (e) => {
        e.preventDefault()

        if(player1.length > 0 && player2.length > 0) {
            fetchPlayers([player1, player2]).then(res => {
                addPlayer(res[0])
                addPlayer(res[1])
                return res
            }).then(res => {
                beganMatch(res)
            })
        }
    }

    const alerts = alerts => {
        return alerts.map((a, i) => <span key={i} className={'alerts ' + a.type}>{a.message}</span>)
    }

    return (
        <form className="form" onSubmit={handleSubmit}>
            { alert.length > 0? alerts(alert) : ''}
            <label className="border-left border-color">
                Player 1:
                <input type="text" {...bindPlayer1} maxLength="9" />
            </label>
            <label>
                Player 2:
                <input type="text" {...bindPlayer2} maxLength="9"/>
            </label>
            <button type="submit" value="Submit" 
                className={player1.length === 0 || player2.length === 0 || alert.length > 0 || isLoad ? 'disable' : ''} 
                disabled={player1.length === 0 || player2.length === 0 || alert.length > 0 || isLoad}>
                { isLoad ? "...loading" : "Start Rumble"}
            </button>
        </form>
    )
}


export default PlayerForm