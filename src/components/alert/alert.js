import React from 'react'
import './alert.scss'

const Alert = props => {
    const { alerts, resetAlerts } = props

    return <ul className="alert">
        {list({alerts, resetAlerts})}
    </ul>
}

const list = props => {
    setTimeout(() => {
        props.resetAlerts()
    }, 10000)

    return props.alerts.map((list, i) => <li key={i + "list"}>{list}</li>)
}


export default Alert