export const ROCK = 'rock'
export const PAPER = 'paper'
export const SISSORS = 'sissors'
export const PLAYER_1 = "player1"
export const PLAYER_2 = "player2"
export const TIE = "tie"


const rockPaperSissors = props  => {
    switch(true) {
        case props.player1Choice === ROCK && props.player2Choice === SISSORS:
        case props.player1Choice === SISSORS && props.player2Choice === PAPER:
        case props.player1Choice === PAPER && props.player2Choice === ROCK:
            return PLAYER_1;
        case props.player2Choice === ROCK && props.player1Choice === SISSORS:
        case props.player2Choice === SISSORS && props.player1Choice === PAPER:
        case props.player2Choice === PAPER && props.player1Choice === ROCK:
            return PLAYER_2;
        default: 
            return TIE
    }
}

export const BattleVeil = props => {
    const [player1Choice, player2Choice] = props.choices
        , result = rockPaperSissors({player1Choice, player2Choice})

    props.callback(result)
}