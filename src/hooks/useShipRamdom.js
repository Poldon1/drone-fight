import { useState } from "react";
import bubble from '../assets/img/ships/bubble-field-ship.png'
import spaceship from '../assets/img/ships/spaceship.png'
import starfighter from '../assets/img/ships/starfighter.png'
import stealthBomber from '../assets/img/ships/stealth-bomber.png'


export const useShipRamdom = () => {
    const ships = [bubble, spaceship, starfighter, stealthBomber]
        , [value, setValue] = useState(ships[Math.floor(Math.random() * ships.length)]);

    return [
        value,
        () => setValue("")
    ]
};